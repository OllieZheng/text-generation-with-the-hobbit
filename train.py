from keras.models import Sequential
from keras.layers import Dense, Activation, LSTM, Dropout
from keras.optimizers import RMSprop
from keras.callbacks import LambdaCallback, ModelCheckpoint
from keras.utils.np_utils import to_categorical

import numpy as np
import pandas as pd
import re
from sklearn.feature_extraction.text import CountVectorizer

import tensorflow as tf

# Load data
with open('the-hobbit.txt', 'r', encoding='utf-8') as f_ptr:
    text = f_ptr.read()

# Clean data
no_newline = ''.join([chara for chara in text if chara != '\n'])
no_chapter = re.sub(r'Chapter (?=[MDCLXVI])M*(C[MD]|D?C*)(X[CL]|L?X*)(I[XV]|V?I*)', '', no_newline)
no_allcaps = re.sub(r'\s*[A-Z]+\s+', '', no_chapter) 

# Get a list of all features and index them
chars = sorted(list(set(no_allcaps)))
char_indices = dict((c, i) for i, c in enumerate(chars))
indices_char = dict((i, c) for i, c in enumerate(chars))

# Create "dataframe" 
seqlen=15
stepsize = 1
X = []
y = []

for i in range(0, len(no_allcaps)-seqlen, stepsize):
    seq = no_allcaps[i: i + seqlen]
    label = no_allcaps[i + seqlen]
    X.append([char_indices[char] for char in seq])
    y.append(char_indices[label])

# Make it digestable by the Keras model
enc_X = np.reshape(X, (len(X), seqlen, 1))
enc_X = enc_X / float(len(chars))
enc_y = to_categorical(y)

# Create Keras model
model = Sequential()
model.add(LSTM(700, 
               input_shape=(enc_X.shape[1], enc_X.shape[2]), 
               return_sequences=True))
model.add(Dropout(0.4))
model.add(LSTM(700, 
               input_shape=(enc_X.shape[1], enc_X.shape[2]), 
               return_sequences=True))
model.add(Dropout(0.4))
model.add(LSTM(700))
model.add(Dropout(0.4))
model.add(Dense(enc_y.shape[1], activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer='adam')
model.summary()

# Feed it through and save it via checkpointer
checkpoint = ModelCheckpoint(filepath='my_model', verbose=1)
model.fit(enc_X, enc_y, epochs=4, batch_size=128, callbacks=[checkpoint])